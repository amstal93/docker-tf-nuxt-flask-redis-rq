
def do_something(res):
    return {'i_haz_result': res}


def predict_model():
    import tensorflow as tf, numpy as np, os
    model_file = os.path.join('/app/models/', 'model.h5')
    custom = {}
    print('here')
    model = tf.keras.models.load_model(model_file)

    print('\n\n\n\n', model)

    dummy_input = np.zeros((28, 28)).reshape((-1, 28, 28))
    predicted   = model.predict(dummy_input)
    predicted = None
    return {
        'results': predicted
    }
